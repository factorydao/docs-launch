.. launch.vote documentation master file, created by
   sphinx-quickstart on Fri Jun 25 13:45:52 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to launch.vote's documentation!
========================================
`launch.vote`_ is a non-custodial crypto auction house that uses novel auction mechanisms to find the optimal price for both fungible and non-fungible digital assets. Auction is designed for price discovery and reaching consensus on the value of new or highly subjective digital assets.

.. _launch.vote: https://launch.factorydao.org/

.. image:: ./images/launch_hz_b.png
   :height: 50px
   :alt: docs-launch missing
   :align: center
   :target: https://launch.factorydao.org/

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   /content/setting_up




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Other products
==================
.. |B| image:: ./images/bank_icon_dark.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/bank/en/latest/

.. |I| image:: ./images/influence_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/influence/en/latest/

.. |M| image:: ./images/markets_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/markets/en/latest/

.. |Y| image:: ./images/yield_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/yield/en/latest/

.. |MI| image:: ./images/mint_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/mint/en/latest/ 
   
* |B| `bank <https://financevote.readthedocs.io/projects/bank/en/latest/>`_
* |I| `influence <https://financevote.readthedocs.io/projects/influence/en/latest/>`_
* |M| `markets <https://financevote.readthedocs.io/projects/markets/en/latest/>`_
* |MI| `mint <https://financevote.readthedocs.io/projects/mint/en/latest/>`_
* |Y| `yield <https://financevote.readthedocs.io/projects/yield/en/latest/>`_

Back to main page
------------------
.. |H| image:: ./images/financevote_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/en/latest/

* |H| `Home <https://financevote.readthedocs.io/en/latest/>`_
