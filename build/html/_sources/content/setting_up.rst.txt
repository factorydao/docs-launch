Setting up
===========

.. _Launch: https://gitlab.com/finance.vote/token-auction

Repository link: `Launch`_

1. In the launch terminal::

    git checkout v2

2. Install dependencies::

    npm install

3. Run application::

    npm start
